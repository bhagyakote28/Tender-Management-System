
#!/bin/bash
 
 
# Set variables
SRC_DIR="src/com/hit"
WEB_CONTENT_DIR="WebContent"
CLASS_DIR="$WEB_CONTENT_DIR/WEB-INF/classes"
LIB_DIR="$WEB_CONTENT_DIR/WEB-INF/lib"
WAR_NAME="app.war"
TOMCAT_LIB_DIR="/opt/tomcat/lib"
 
# Clean previous build artifacts
rm -rf $CLASS_DIR
rm -rf $LIB_DIR
rm -f $WAR_NAME
 
 
# Create necessary directories
 
mkdir -p $CLASS_DIR
mkdir -p $LIB_DIR
 
# Compile Java files with servlet API in classpath
javac -d $CLASS_DIR -sourcepath $SRC_DIR -cp "$TOMCAT_LIB_DIR/servlet-api.jar" $(find $SRC_DIR -name "*.java")
 
# Copy resources if they exist
if [ -d "resources" ]; then
  cp -r resources/* $CLASS_DIR/
fi
 
# Create the WAR file
cd $WEB_CONTENT_DIR
jar cvf ../$WAR_NAME *
cd ..
 
# Output the result
echo "WAR file $WAR_NAME created successfully."